package ru.bokhan.tm.command.user;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.util.TerminalUtil;

public class ProfileUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "profile-update";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Update my profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().updateById(
                userId, login,
                firstName, lastName, middleName,
                email
        );
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
