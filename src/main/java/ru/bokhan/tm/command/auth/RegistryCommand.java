package ru.bokhan.tm.command.auth;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.util.TerminalUtil;

public class RegistryCommand extends AbstractCommand {

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK:]");
    }

}

