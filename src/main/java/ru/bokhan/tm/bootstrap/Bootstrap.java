package ru.bokhan.tm.bootstrap;

import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.*;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.command.auth.LoginCommand;
import ru.bokhan.tm.command.auth.LogoutCommand;
import ru.bokhan.tm.command.auth.RegistryCommand;
import ru.bokhan.tm.command.project.*;
import ru.bokhan.tm.command.system.*;
import ru.bokhan.tm.command.task.*;
import ru.bokhan.tm.command.user.PasswordUpdateCommand;
import ru.bokhan.tm.command.user.ProfileUpdateCommand;
import ru.bokhan.tm.command.user.ProfileViewCommand;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.system.UnknownCommandException;
import ru.bokhan.tm.repository.ProjectRepository;
import ru.bokhan.tm.repository.TaskRepository;
import ru.bokhan.tm.repository.UserRepository;
import ru.bokhan.tm.service.AuthService;
import ru.bokhan.tm.service.ProjectService;
import ru.bokhan.tm.service.TaskService;
import ru.bokhan.tm.service.UserService;
import ru.bokhan.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void initCommands() {
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new RegistryCommand());

        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectViewByIdCommand());
        registry(new ProjectViewByIndexCommand());
        registry(new ProjectViewByNameCommand());

        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskViewByIdCommand());
        registry(new TaskViewByIndexCommand());
        registry(new TaskViewByNameCommand());

        registry(new PasswordUpdateCommand());
        registry(new ProfileUpdateCommand());
        registry(new ProfileViewCommand());
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initData() {
        final User user = userService.create("test", "test", "test@test.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(user.getId(), "UserTask1");
        taskService.create(user.getId(), "UserTask2");
        taskService.create(admin.getId(), "AdminTask1");
        taskService.create(admin.getId(), "AdminTask2");
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
    }

    public void run(final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArguments(arguments)) System.exit(0);
        initCommands();
        initData();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        final AbstractCommand command = getCommandByArgument(argument);
        if (command == null) return;
        command.execute();
    }

    public void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public boolean parseArguments(final String[] arguments) {
        if (arguments == null || arguments.length == 0) return false;
        final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    private AbstractCommand getCommandByArgument(final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (final AbstractCommand command : commands.values()) {
            if (argument.equals(command.argument())) return command;
        }
        return null;
    }

}